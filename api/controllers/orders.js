const mongoose = require('mongoose');
const Order = require('../models/order');

exports.getAllOrdes = (req, res, next) => {
  Order.find().select('_id product quantity')
     .populate('product', 'name') //adicionar informação extra sobre o produto adicionado a encomenda
     .exec()
     .then(docs => {
        const response = {
           count: docs.length,
           orders: docs.map(doc => {
              return {
                 _id: doc._id,
                 product: doc.product,
                 quantity: doc.quantity,
                 request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders/' + doc._id+ ''
                 }
              }
           })
        };
        res.status(200).json(response);
     })
     .catch(err => {
        res.status(500).json(err);
     });
}

exports.postOrder = (req, res, next) => {
   const order = new Order({
     _id: mongoose.Types.ObjectId(),
     quantity: req.body.quantity,
     product: req.body.productId
   });

   order.save().then(result => {
     console.log(result);
     res.status(201).json(result);
   }).catch(err => {
     console.log(err);
     res.status(500).json({
       error: err
     });
   });

}

exports.findOrderById = (req, res, next) => {
   const id = req.params.orderId;

   Order.findById(id).select('_id product quantity')
      .exec()
      .then(doc => {
         if (doc) {
            res.status(200).json({
               _id: doc._id,
               product: doc.product,
               quantity: doc.quantity
            });
         } else {
            res.status(404).json({message: 'No valid Entry found for provided ID'});
         }
      })
      .catch(err => {
         res.status(500).json({"error": err});
      });
}

exports.deleteOrder = (req, res, next) => {
   const id = req.params.orderId;

   Order.findOneAndRemove({_id: id}).exec()
      .then(result =>{
         res.status(200).json(result);
      })
      .catch(err => {
         res.status(500).json({error: err});
      });
}
