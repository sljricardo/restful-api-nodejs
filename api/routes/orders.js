const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/check-auth');
const ordersController = require('../controllers/orders');


router.get('/', checkAuth , ordersController.getAllOrdes);

router.post('/', checkAuth, ordersController.postOrder);

router.get('/:orderId', checkAuth, ordersController.findOrderById);

router.delete('/:orderId', checkAuth , ordersController.deleteOrder);

module.exports = router;
