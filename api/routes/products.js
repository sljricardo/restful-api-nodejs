const express = require('express');
const productSchema = require('../models/product');
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');

//config to store a file
const storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null, './uploads/');
  },
  filename: function(req, file, cb){
    cb(null, Date.now() + '_' + file.originalname );
  }
});

const fileFilter = (req, file, cb) => {

  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    //accept file
    cb(null, true);
  } else {
    //reject file
    cb(null, false);
  }
}

//parametros de gravação mais limite maximo de upload
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5 // 1024 * 1024 = 1 Mb * 5 = 5Mb
  },
  fileFilter: fileFilter
});
const router = express.Router();

//return all products
router.get('/', (req, res, next) => {
   productSchema.find().select('name price _id productImage')
      .exec()
      .then(docs => {
         const response = {
            count: docs.length,
            products: docs.map(doc => {
               return {
                  name: doc.name,
                  price: doc.price,
                  productImage: doc.productImage,
                  _id: doc._id,
                  request: {
                     type: 'GET',
                     url: 'http://localhost:3000/products/' + doc._id+ ''
                  }
               }
            })
         };
         res.status(200).json(response);
      })
      .catch(err => {
         res.status(500).json(err);
      });
});

//storing a product //add store image to product throw form
router.post('/', checkAuth , upload.single('productImage') ,(req, res, next) => {
    console.log(req.file);
   const product = new productSchema({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      price: req.body.price,
      productImage: req.file.path
   });
   product.save()
      .then(result => {
         res.status(201).json({
            message: "Created product success",
            createdProduct: {
               name: result.name,
               price: result.price,
               productImage: result.productImage,
               _id: result._id,
               request: {
                  type: 'GET',
                  url: 'http://localhost:3000/products/' + result._id+ ''
               }
            }
         });
      })
      .catch(err => {
         res.status(500).json({"error": err});
      });
});

//search product for id
router.get('/:productId', (req, res, next) => {
   const id = req.params.productId;

   productSchema.findById(id).select('name price _id productImage')
      .exec()
      .then(doc => {
         if (doc) {
            res.status(200).json({
               name: doc.name,
               price: doc.price,
               produtctImage: doc.productImage,
               _id: doc._id
            });
         } else {
            res.status(404).json({message: 'No valid Entry found for provided ID'});
         }
      })
      .catch(err => {
         res.status(500).json({"error": err});
      });
});


//update product for id
router.patch('/:productId', checkAuth ,(req, res, next) => {
   const id = req.params.productId;
   const updateOps = {};

   //for each element on body request add to object
   /*
      REQUEST exemple
      [
      	{ "propName": "name", "value": "Harry Potter 45" },
      	{ "propName": "price", "value": "45" }
      ]
   */
   for(const ops of req.body){
      updateOps[ops.propName] = ops.value;
   }

   productSchema.update({_id: id}, { $set: updateOps })
      .exec()
      .then(result =>{
         res.status(200).json(result);
      })
      .catch(err => {
         res.status(500).json({error: err});
      });
});


//Delete product for id
router.delete('/:productId', checkAuth ,(req, res, next) => {
   const id = req.params.productId;

   productSchema.findOneAndRemove({_id: id}).exec()
      .then(result =>{
         res.status(200).json(result);
      })
      .catch(err => {
         res.status(500).json({error: err});
      });
});

module.exports = router;
