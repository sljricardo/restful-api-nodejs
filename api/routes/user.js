const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/signup', (req, res, next) => {
  //verify if email already exists in DB
  User.find({email: req.body.email})
      .exec()
      .then(user => {
        if(user.length >= 1){
          return res.status(409).json({
            warning: "Email already exists"
          });
        } else {
          //hash password and save email and pass to DB
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).json({ error: err });
            } else {
              const user = new User({
                  _id: new mongoose.Types.ObjectId(),
                  email: req.body.email,
                  password: hash
              });
              user.save()
              .then(result => {
                console.log(result);
                res.status(201).json({
                  message: 'User created'
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
            }
          });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
});

router.post('/login', (req,res,next) => {
  User.find({email: req.body.email})
      .exec()
      .then( users => {
        if (users.length < 1) {
          return res.status(401).json({
            message: 'Auth failed'
          });
        } else {
          bcrypt.compare(req.body.password, users[0].password, function(err, result) {
              if (err) {
                return res.status(401).json({
                  message: 'Auth failed'
                });
              }
              if (result) {
                //token generator
                const token = jwt.sign({
                  email: users[0].email,
                  userId: users[0]._id
                },
                process.env.JWT_PRIVATE_KEY,
                {
                  expiresIn: "1h"
                });

                return res.status(200).json({
                  message: 'Auth successful',
                  token: token
                });
              } else {
                return res.status(401).json({
                  message: 'Auth failed'
                });
              }
          });
        }
      })
      .catch( err => {
        res.status(404).json({ error: err });
      });
});

router.delete('/:idUser', (req, res, next) => {
  User.findOneAndRemove({_id: req.params.idUser})
      .exec()
      .then(result => {
        res.status(200).json({
          message: "User delete"
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
});

module.exports = router;
