const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

const products = require('./api/routes/products');
const orders = require('./api/routes/orders');
const user = require('./api/routes/user');

//mongoDb connection atlas shareHost
mongoose.connect(''+ process.env.CONNECTION_VAR +'');

//log handler to app
app.use(morgan('dev'));

//static folder
app.use('/uploads', express.static('uploads'));

//Parse Body Parse
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Handling CORS errors is important to a RESTful API
   //... * if we whant to give access to all requests
   //... Or for exemple http://www.mypage.com if we only want o give access to our page.
app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
   if (req.method == 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
      res.status(200).json({});
   }
   next();
});

//routes to handle requests
app.use('/products', products);
app.use('/orders',  orders);
app.use('/user',  user);

app.use((req, res, next) => {
   const error = new Error('Not found');
   error.status = 404;
   next(error);
});

// trigger code send an error
app.use((err, req, res, next) => {
   res.status(err.status || 500).json({
      error: {
         message: err.message
      }
   });
});

module.exports = app;
