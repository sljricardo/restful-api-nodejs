# RESTful-API-nodeJs

### Functionality

 This whas a simple project only to create a Rest API in node, the API can handle requests to products and orders, with the possibility to **add**, **search**, **update** and **delete** the information.
 
### Installation

 1 - Clone the repo for your local machine<br>
 2 - Run the fallowing commands in the folder where you have the project $ **npm install**<br>
 3 - Change the variable **process.env.CONNECTION_VAR** in file ficheiro app.js with wanted connection, (Host or Local)<br>
 
### Requirements

 - NodeJs<br>
 - npm<br>
 - mongoDB<br>